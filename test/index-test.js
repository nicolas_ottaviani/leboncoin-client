var chai = require('chai'),
    chaiAsPromised = require("chai-as-promised");
var leboncoin = require('../index');

chai.use(chaiAsPromised);
var expect = chai.expect,
    should = chai.should()

describe('leboncoin-client', function(){
  describe('global test', function(){
    it('should parse getAll', function (){
      return leboncoin.getAll().should.eventually.have.deep.property('pagination.page', 1)
    });
  });
});
