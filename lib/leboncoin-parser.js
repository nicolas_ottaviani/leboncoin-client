var cheerio = require('cheerio');
var _eval = require('eval')

/**
 * parse leboncoint html page
 *
 * @name parseGetAll
 * @private
 * @function
 * @param {String} html html page
 * @return {Object} pagination properties and ids colllection
 */
function parseGetAll (html){
  var $ = cheerio.load(html);
  var props = parsePropsScript ($('body script').filter (function (i, el){
    return $(el).text().indexOf('utag_data') >= 0;
  }));
  return {
      pagination:{
        page: parseInt(props.pagenumber),
        pageSize: parseInt(props.nbresultat_displayed),
        total: parseInt(props.nbresultat),
        pageCount: Math.floor(parseInt(props.nbresultat) / parseInt(props.nbresultat_displayed))
      },
      collection: $('#listingAds > section > section > ul > li > a').map(function (i, el){
        return {
          id: parseAdId(el)
        }
      }).get()
  };
}

/**
 * parse leboncoint html page
 *
 * @name parsetGet
 * @private
 * @function
 * @param {String} html html page
 * @return {Object} leboncoin add properties
 */
function parseGet (html){
  var $ = cheerio.load(html);
  var props = parsePropsScript ($('body script').filter (function (i, el){
    return $(el).text().indexOf('utag_data') >= 0;
  }));
  var images = parseImagesSrcipt($('p.item_photo').next('script'));
  var name = $('#adview > section > header > h1').text().trim();
  var author = $('#adview > section > section > section.properties.lineNegative > div.line.line_pro.noborder > p > a').text().trim();
  var description = $('#adview > section > section > section.properties.lineNegative > div.line.properties_description > p.value').html();
  return {
    id: props.listid,
    name: name,
    metadata: {
      author: author,
      category: props.cat,
      subCategory: props.subcat,
      region: props.region,
      department: props.departement,
      cp: props.cp,
      city: props.city,
      offres: props.offres,
      publishDate: parseDate (props.publish_date),
      lastUpdateDate: parseDate (props.last_update_date),
      price: props.prix,
      numberOfRooms: props.pieces,
      floorSize: props.surface,
      type: props.type,
      ges: props.ges,
      energy: props.nrj
    },
    images: images,
    description: description
  };
}

function parseImagesSrcipt ($script){
  var script = $script.text()
  if (script){
    script = script.substring(0, script.indexOf('var interstitial_active'))
    var images = _eval(script+';exports.images_thumbs = images_thumbs', true).images_thumbs
    return images.map(mapImageHref);
  }
  return [];
}

function parsePropsScript ($script){
  var script = $script.text()
    .replace(/device.*[\r\n]+/g, "")
    .replace(/displaytype.*[\r\n]+/g, "")
    .replace(/uab.*[\r\n]+/g, "");
  return _eval(script+'exports.utag_data = utag_data;', true).utag_data
}

function parseDate (date){
  return date;
}

function mapImageHref (str){
  return str.substring(str.lastIndexOf('/')+1);
}

function parseAdId (a){
  var dataInfo = a.attribs['data-info'];
  if (!!!dataInfo){
    return null;
  }
  var dataInfo = JSON.parse(a.attribs['data-info']);
  return dataInfo.ad_listid;
}

module.exports = {
  parseGet: parseGet,
  parseGetAll: parseGetAll
};
