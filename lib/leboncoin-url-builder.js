var util = require('util')
var querystring = require('querystring');


/**
 * build getAll url
 *
 * @name buildGetAllUrl
 * @private
 * @function
 * @param {String} url leboncoin url
 * @param {Object} params params with
 * @return {String} pagination getAll url
 */
function buildGetAllUrl (url, params){
  if (!params){
    params = {}
  }

  var region = params.region;
  var category = params.category || 'annonces';
  var department = params.department;
  var offres = params.offres;
  if (region){
    if (department){
      url += util.format('/%s/offres/%s/%s/', category, region, department);
    }
    else{
      url += util.format('/%s/offres/%s/', category, region);
    }
  }
  else{
    url+= util.format('/%s/offres/', category);
  }

  var apiParams = {};
  if (offres === "pro"){
    apiParams.f = 'c';
  }
  else if (offres === "part"){
    apiParams.f = 'p';
  }
  if (params.page){
    apiParams.o = params.page;
  }
  return url+'?'+querystring.stringify(apiParams);
}

/**
 * build get url
 *
 * @name buildGetUrl
 * @private
 * @function
 * @param {String} url leboncoin url
 * @param {String} id ad id
 * @return {String} pagination get url
 */
function buildGetUrl (url, id){
  return url+util.format('/annonces/%s.htm', id);
}


module.exports = {
  buildGetAll: buildGetAllUrl,
  buildGet: buildGetUrl
};
