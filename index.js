
var request = require('request-promise');
var iconv = require('iconv-lite');
var Promise = require('promise');

var parser = require('./lib/leboncoin-parser');
var urlBuilder = require('./lib/leboncoin-url-builder');

var url = 'https://www.leboncoin.fr';

/**
 * get all ads ids
 *
 * @name getAll
 * @function
 * @param {Object} params params with
 * @return {Object} ads id collection
 */
function getAll(params){
  return request ({"uri": urlBuilder.buildGetAll(url, params), "encoding": null})
    .then (decode)
    .then (parser.parseGetAll)
    .catch(rejectError);
}

/**
 * get ad
 *
 * @name get
 * @function
 * @param {String} id ad id
 * @return {Object} ad
 */
function get(id){
  return request ({"uri": urlBuilder.buildGet(url, id), "encoding": null})
    .then (decode)
    .then (parser.parseGet)
    .catch(rejectError);
}

function rejectError (error){
  return Promise.reject(error);
}

function decode (body){
  return iconv.decode(body, 'iso-8859-1')
}


module.exports = {
  getAll: getAll,
  get: get
};
